//
// Created by Adam Karczewski on 29/10/2018.
//

#include "BB.h"
#include "Queue.h"
#include<math.h>
#include <iostream>
#include <fstream>
using namespace std;

BB::BB(Graph* graph, bool bfs) {
    this->graph=graph;
    this->bfs=bfs;
    string str;
    if(bfs) str="wynikiBB_bfs.txt";
    else str="wynikiBB.txt";
    finalPath = new int[graph->getSize()+1];
    visted= new bool[graph->getSize()];
    fstream plik(str, ios::app);
    if (!plik.good()) throw "Blad otwarcia pliku";
    plik<<graph->getSize()<<" ";
    std::cout<<"Rozmiar grafu: "<<graph->getSize()<<std::endl;
    double suma = 0;
    for(int i=0; i<5; i++)
    {
        std::cout<<"Powtorzenie numer: "<<i<<std::endl;
        auto start = chrono::high_resolution_clock::now();
        calculateBB();
        auto end = chrono::high_resolution_clock::now();
        chrono::duration<double> diff = end - start; //obliczam czas trawania operacji
        suma += diff.count();
        std::cout<<"Czas: "<<diff.count()<<endl;
        std::cout<<"Koszt: "<< finalWeight<<endl;
        std::cout<<"Sciezka: "<<std::endl;
        for(int i=0; i<graph->getSize()+1; i++)
            cout<<finalPath[i]<<" ";
        std::cout<<std::endl<<"........................."<<std::endl;
    }

    plik<< suma/5<<std::endl;
}

BB::~BB() {
    delete [] finalPath;
    delete[] visted;
}

void BB::calculateBB(){
    int* path = new int[graph->getSize()+1];  //ścieżka - numery wierzcholkow
    int bound = 0;  // ograniczenie
    for(int i=0; i<(graph->getSize()+1);i++) path[i]=-1;
    for(int i=0; i<graph->getSize();i++) visted[i]=false; //inicjalizacja tablic

    //obliczenie pierwotnego ograniczenia
    for(int i=0; i<graph->getSize(); i++){
        bound+=graph->getMinEdge(i,false)+graph->getMinEdge(i,true);
    }

    bound= (int)(round(bound/2));   //obliczenie dolnego ogranicznia

    //zaczynamy od wierzchołka 0
    visted[0] = true;
    path[0] = 0;

    if(!bfs) BBrecursion(bound,0,1,path);
    else BBrecursionBFS(bound,0,1,path);





    delete[] path;
}

/*
 bound - aktualne ograniczenie
 weight - aktualna waga dotychczasowej sciezki
 level - poziom drzewa rozwiazan
 path - tablica aktulanej sciezki
 visted - tablica odwiedzonych
 */
void BB::BBrecursion(int bound, int weight, int level, int *path) {

    // jezeli przeszlismy na ostatni poziom drzewa - warunek zakonczenie rekurencji
    if(level==graph->getSize()){
        // jezeli jest sciezka do powrotu
        if(graph->getDistance(path[level-1],path[0])!=0){
            int completeWeight = weight + graph->getDistance(path[level-1],path[0]);

            if(completeWeight<finalWeight){
                makeSolution(path);
                finalWeight= completeWeight;
            }
        }
        return;
    }

    // wszystkie inne przypadki
   for(int i=0; i<graph->getSize();i++){
       // jesli nie odwiedzielismy wierzchoklka i krawedz istnieje
       if(visted[i]== false){
           int tempBound = bound;
           weight += graph->getDistance(path[level-1],i);  // dodaje nowa krawedz do wagi

           //obliczenie nowego ograniczenia
          if(level==1) bound-=(graph->getMinEdge(0,true)+graph->getMinEdge(i,false))/2;
          else bound-=(graph->getMinEdge(i,true)+graph->getMinEdge(i,false))/2;

           // jezeli ograniecznie jest mniejsze od dotychczasowego minimum -> idziemy ta drogą
           if(bound + weight < finalWeight){
                path[level] = i;
                visted[i] = true;

                BBrecursion(bound,weight,level +1,path);
           }
           //cofniecie wprowadoznych zmina
           weight -= graph->getDistance(path[level-1],i);
           bound = tempBound;
          for(int j=0; j<graph->getSize();j++) visted[j]=false;
           for(int j=0; j<level;j++)
               visted[path[j]] = true;
       }
   }


}

void BB::BBrecursionBFS(int bound, int weight, int level, int *path) {

    // jezeli przeszlismy na ostatni poziom drzewa - warunek zakonczenie rekurencji
    if(level==graph->getSize()){
        // jezeli jest sciezka do powrotu
        if(graph->getDistance(path[level-1],path[0])!=0){
            int completeWeight = weight + graph->getDistance(path[level-1],path[0]);

            if(completeWeight<finalWeight){
                makeSolution(path);
                finalWeight= completeWeight;
            }
        }
        return;
    }
    Queue queue(graph->getSize());
    // wszystkie inne przypadki
    for(int i=0; i<graph->getSize();i++){
        // jesli nie odwiedzielismy wierzchoklka i krawedz istnieje
        if(visted[i]== false){
            int tempBound = bound;
            weight += graph->getDistance(path[level-1],i);  // dodaje nowa krawedz do wagi

            if(level==1) bound-=(graph->getMinEdge(0,true)+graph->getMinEdge(i,false))/2;
            else bound-=(graph->getMinEdge(i,true)+graph->getMinEdge(i,false))/2;

            // jezeli ograniecznie jest mniejsze od dotychczasowego minimum -> idziemy ta drogą
            if(bound + weight < finalWeight){
                Verticle e;
                e.v=i;
                e.bound=bound;
              queue.push(e);
            }
            //cofniecie wprowadoznych zmina
            weight -= graph->getDistance(path[level-1],i);
            bound = tempBound;
        }
    }

   while(queue.getSize()>0){
        Verticle v = queue.getFront();
        queue.pop();
        path[level] = v.v;
        visted[v.v] = true;
        BBrecursionBFS(v.bound,weight+graph->getDistance(path[level-1],v.v),level+1,path);
        for(int j=0; j<graph->getSize();j++) visted[j]=false;
        for(int j=0; j<level;j++)
            visted[path[j]] = true;
    }
}

void BB::makeSolution(int *path) {
    for(int i=0; i<graph->getSize();i++)
        finalPath[i]=path[i];
    finalPath[graph->getSize()]= path[0];
}

