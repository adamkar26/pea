//
// Created by Adam Karczewski on 29/10/2018.
//
#include "Graph.h"
#ifndef PEA1_BB_H
#define PEA1_BB_H


class BB {
private:
    void BBrecursion(int bound, int weight, int level, int* path);
    void calculateBB();
    void makeSolution(int* path );
    void BBrecursionBFS(int bound,int weight,int level, int* path);

    Graph* graph;
    int finalWeight = INT_MAX;
    int* finalPath;
    bool b = false;
    bool* visted = &b;
    bool bfs;

public:
     BB(Graph* graph, bool bfs);
     ~BB();

};


#endif //PEA1_BB_H
