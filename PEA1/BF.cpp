//
// Created by Adam Karczewski on 18/10/2018.
//

#include "BF.h"
#include <cstdlib>
#include<iomanip>
#include <chrono>
#include <fstream>
#include <iostream>

BF:: BF(Graph* graph)
{
this->graph=graph;
fstream plik("wynikiBF.txt", ios::app);
if (!plik.good()) throw "Blad otwarcia pliku";
plik<<graph->getSize()<<" ";
std::cout<<"Rozmiar grafu: "<<graph->getSize()<<std::endl;
double suma = 0;
for(int i=0; i<5; i++)
{
    std::cout<<"Powtorzenie numer "<<i<<std::endl;
    auto start = chrono::high_resolution_clock::now();
    calculateBF();
    auto end = chrono::high_resolution_clock::now();
    chrono::duration<double> diff = end - start; //obliczam czas trawania operacji
    suma += diff.count();
    std::cout<<"czas: "<<diff.count()<<endl;
    std::cout<<"ścieżka: ";
    if(sptr)
    {
        for(int j=0; j<sptr; j++) std::cout<<heap[j]<<" ";
        std:: cout<<startV<<std:: endl;
        std:: cout<<"path: "<<path<<std:: endl;
    } else std::cout<<" Brak cyklu hamiltona"<<std::endl;
    std::cout<<"........................."<<std::endl;
}
plik<< suma/5<<std::endl;

}

BF::~BF() {
    delete [] heap;
    delete [] heap1;
    delete [] visited;
 }

void BF::calculateBF() {
     path= 2147483647;  //dlugosc sciezki - MAX INT
     actual_path = 0;
     startV = rand()%graph->getSize();
     heap = new int[graph->getSize()];
     heap1 = new int[graph->getSize()];
     sptr = 0;
     sptr1 = 0;
     visited = new bool[graph->getSize()];
     for(int i=0 ;i<graph->getSize(); i++) visited[i]= false;
    ATSP(startV);
}

void BF::ATSP(int v) {

    heap1[sptr1++] = v;  //zapamietuje wierzchołek na stosie

    if(sptr1 < graph->getSize()) //jeżeli nie ma jeszcze cyklu
    {
        visited[v] = true;  // wierzcholek oznaczam jako odwiedzony
        for(int i=0; i<graph->getSize(); i++) //szukamy sasiadow
        {
            if(graph->getDistance(v,i)!=0 && !visited[i] )//jesli wierzcholek nieodwiedzony
            {
                actual_path+= graph->getDistance(v,i); //dodaje do sciezki krawedz
                ATSP(i); //wywolanie rekurencyjne
                actual_path -= graph->getDistance(v,i); //usuwam krawedz
            }
        }
        visited[v] = false; //zwalniam wierzcholek
    }
    else if (graph->getDistance(startV,v))  //jesli znaleziona sciezka jest cyklem
    {
        actual_path += graph->getDistance(v, startV); //dodajemy powrot na miejsce startu
        if(actual_path<path)   //znalezlismy najkrotsza (aktulanie) sciezke
        {
            path = actual_path;
            for(int i =0; i<sptr1; i++)
            {
                heap[i] = heap1[i];     // kopiuje wierzcholki na stos wyniku
            }
            sptr = sptr1;
        }
        actual_path -= graph->getDistance(v, startV);  // usuwam dodana krawedz
        
    }
    sptr1--;
}