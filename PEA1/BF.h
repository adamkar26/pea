//
// Created by Adam Karczewski on 18/10/2018.
//

#ifndef PEA1_BF_H
#define PEA1_BF_H

#include "Graph.h"

class BF {

    Graph* graph;
    int path= INT_MAX;  //dlugosc sciezki - MAX INT
    int actual_path;
    int startV;
    int* heap;
    int* heap1;
    int sptr;
    int sptr1;
    bool* visited;
    public: BF(Graph* graph);
    public: ~BF();
    public: void calculateBF();
    private: void ATSP(int v);
};


#endif //PEA1_BF_H
