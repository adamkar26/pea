//
// Created by Adam Karczewski on 12/11/2018.
//

#include "Queue.h"

Queue::Queue(int n)
{
    heap = new Verticle[n];  // alokacja pamieci
    endHeap = 0;
}

Queue::~Queue()
{
    delete[] heap;
}

Verticle Queue::getFront()
{
    return  heap[0];

}

int Queue::getSize() {
    return endHeap;
}

void Queue::clear() {
    endHeap=0;
}
void Queue::push(Verticle e)
{
    int i, j;
    i = endHeap; //i na koncu kopca
    j = (i - 1) / 2;  // rodzic
    endHeap++; //zwiekszam koniec kopca
    while (i != 0 && (heap[j].bound > e.bound))
    {
        heap[i] = heap[j];
        i = j;
        j = (i - 1) / 2;
    }
    heap[i] = e;
}

void Queue::pop()
{
    int i, j;
    Verticle e;
    if (endHeap)
    {
        e = heap[--endHeap];
        i = 0;
        j = 1;
        while (j<endHeap)
        {
            if ((j + 1 < endHeap) && (heap[j + 1].bound < heap[j].bound)) j++;
            if (e.bound <= heap[j].bound) break;
            heap[i] = heap[j];
            i = j;
            j = 2*j+1;
        }
        heap[i] = e;

    }

}
