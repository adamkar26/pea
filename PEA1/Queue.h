//
// Created by Adam Karczewski on 12/11/2018.
//

#ifndef PEA1_QUEUE_H
#define PEA1_QUEUE_H

struct Verticle
{
    int v, bound;
};

class Queue {

private:
    Verticle* heap; // kopiec
    int endHeap; //ostatnia pozycja (indeks kopca)


public:
    Queue(int n);
    ~Queue();
    Verticle  getFront();
    void push(Verticle e);
    void pop();
    int getSize();
    void clear();
};


#endif //PEA1_QUEUE_H
