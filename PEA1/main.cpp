#include <iostream>
#include <fstream>
#include "Graph.h"
#include "BF.h"
#include "BB.h"
 using namespace std;

void test(){
    int t;
    BF* bf;
    BB* bb;
    string path;
    cout<<"Podaj ścieżkę do pliku: ";
    cin>>path;
    Graph*  graph = new Graph(path);
    cout<< "Rodzaj algorytmu: "<<endl;
    cout<<"1. BF"<<endl;
    cout<<"2. B&B  breadth first search"<<endl;
    cout<<"3. B&B best first search"<<endl;
    cin>>t;
    switch (t){
        case 1:
            bf = new BF(graph);
            delete bf;
            break;

        case 2:
            bb = new BB(graph,false);
            delete bb;
            break;

        case 3:
            bb = new BB(graph,true);
            delete bb;
            break;



    }


    delete graph;
}

void measure(){
    int t=0;
    string s;
    BB* bb;
    BF* bf;
    vector<Graph*> graphs;
    ifstream file;
    file.open("conf.txt");
    if (!file.good()) perror(nullptr);
    while(!file.eof()){
        file>>s;
        graphs.push_back(new Graph(s));
    }

    cout<< "Rodzaj algorytmu: "<<endl;
    cout<<"1. BF"<<endl;
    cout<<"2. B&B  breadth first search"<<endl;
    cout<<"3. B&B best first search"<<endl;
    cin>>t;
    switch (t){
        case 1:
            for(int i=0; i<graphs.size(); i++)
            {
                bf = new BF(graphs[i]);
                delete bf;

            }
            break;

        case 2:
            for(int i=0; i<graphs.size(); i++)
            {
                bb = new BB(graphs[i], false);
                delete bb;

            }
            break;

        case 3:
            for(int i=0; i<graphs.size(); i++)
            {
                bb = new BB(graphs[i], true);
                delete bb;

            }
            break;

    }
    graphs.clear();


}
void cmd() {
    int t=0;
    while(t!=3)
    {
        cout << "Wybierz tryb pracy: " << endl;
        cout << "1. test poprawności " << endl;
        cout << "2. pomiary " << endl;
        cout << "3. koniec "<<endl;
        cin >> t;
        switch (t) {
            case 1 : test(); break;
            case 2 : measure(); break;
        }
    }
}



int main() {
    cmd();
    return 0;
}



