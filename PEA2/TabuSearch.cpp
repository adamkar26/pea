//
// Created by Adam Karczewski on 2018-12-01.
//

#include "TabuSearch.h"

TabuSearch::~TabuSearch(){
    for(int i=0;i<graph->getSize();i++)
        delete[] tabuList[i];
    delete[] tabuList;
}

int TabuSearch::solve() {

    string path="./wyniki_"+to_string(graph->getSize())+".csv";
    fstream file(path, ios::out);
    auto start = std::chrono::high_resolution_clock::now();
    prepare();
    auto end = chrono::high_resolution_clock::now();
    chrono::duration<double> diff = end - start;
    file<<diff.count()<<", "<<bestPathValue<<endl;
    int criticalEvents=0;
    for(int i=0;i<iteration;i++){

        // sprawdzenie warunku czasu;
        auto end = chrono::high_resolution_clock::now();
        chrono::duration<double> diff = end - start;
        if(diff.count()>maxTime) break;
        decrement();
        generatePermutation();
        if(actualPathValue<bestPathValue)
        {
            bestPath=actualPath;
            bestPathValue=actualPathValue;
            auto end = chrono::high_resolution_clock::now();
            chrono::duration<double> diff = end - start;
            file<<diff.count()<<", "<<bestPathValue<<endl;
        }
        else
        {
            criticalEvents++;
            //dewersyfikacja
            if(criticalEvents>maxCriticalEvents) {
                std::random_device rd;
                std::mt19937 g(rd());
                std::shuffle(actualPath.begin(),actualPath.end(),g);
                actualPathValue=calculatePath(actualPath);
                criticalEvents=0;
            }

        }

    }
    file.close();
   return bestPathValue;
}


void TabuSearch::prepare() {

    for(int i=0; i<graph->getSize();i++) actualPath.push_back(i);

    // losowy start
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(actualPath.begin(),actualPath.end(),g);

    bestPath=actualPath;
    actualPathValue=calculatePath(actualPath);
    bestPathValue=actualPathValue;

    // inicjalizacnia listy tabu
    tabuList=new int*[graph->getSize()];
    for(int i=0;i<graph->getSize();i++)
    {
        tabuList[i]=new int[graph->getSize()];
        for(int j=0; j<graph->getSize();j++)
            tabuList[i][j]=0;
    }


}

int TabuSearch::calculatePath(vector<int> path) {

    int value=0;
    for(int i=1;i<path.size();i++)
        value+=graph->getDistance(path[i-1],path[i]);
    value+=graph->getDistance(path[graph->getSize()-1],path[0]);
    return value;
}

void TabuSearch::decrement() {

    for(int i=0;i<graph->getSize();i++)
        for(int j=0;j<graph->getSize();j++)
        {
            if(tabuList[i][j]>0) tabuList[i][j]--;
        }
}

void TabuSearch::generatePermutation() {

    int valueOfTheBest= INT_MAX;
    vector<int> bestFind;
    int bestI, bestJ;
    int tabuExam =0;
    bool first=false;
    int firstIndex=0;

    for(int i=0;i<graph->getSize();i++)
    {
        for(int j=0; j<graph->getSize();j++){
            if(tabuList[i][j]==0){
                vector<int> temp;
                temp = actualPath;
                swap(temp[i],temp[j]);
                int tempValue=calculatePath(temp);
                if(tempValue<valueOfTheBest)
                {
                    bestFind=temp;
                    valueOfTheBest=tempValue;
                    bestI=i;
                    bestJ=j;
                }

            }
            else if(tabuExam<aspirationMax)
            {
                tabuExam++;
                vector<int> temp;
                temp = actualPath;
                swap(temp[i],temp[j]);
                int tempValue=calculatePath(temp);
                if(tempValue<bestPathValue)
                {
                    bestFind=temp;
                    valueOfTheBest=tempValue;
                    bestI=i;
                    bestJ=j;
                    first=true;
                    firstIndex++;
                }
                if(first && firstIndex>aspirationPlus && tabuExam>aspirationMin)
                    tabuExam=aspirationMax;
            }

        }
    }
    actualPath=bestFind;
    actualPathValue=valueOfTheBest;
    tabuList[bestI][bestJ]=lifetime;
}


