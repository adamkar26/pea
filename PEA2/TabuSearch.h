//
// Created by Adam Karczewski on 2018-12-01.
//

#ifndef PEA2_TABUSEARCH_H
#define PEA2_TABUSEARCH_H

#include "Graph.h"
#include <chrono>
#include <iostream>
#include <random>
#include <fstream>

class TabuSearch {

private:

    Graph* graph;
    int iteration;
    int maxCriticalEvents;
    double maxTime;
    int aspirationMin;
    int aspirationMax;
    int aspirationPlus;
    int lifetime;

    vector<int> bestPath;
    int** tabuList;
    int bestPathValue;

    vector<int> actualPath;
    int actualPathValue;

    void prepare();
    int calculatePath(vector<int> path);
    void decrement();
    void generatePermutation();



public:
    TabuSearch(Graph*graph,int iteration, int  maxCritalEvents, double maxTime,
            int aspirationMin, int aspitationMax,int aspirationPlus, int lifetime): graph(graph), iteration(iteration),
            maxCriticalEvents(maxCritalEvents),maxTime(maxTime),aspirationMin(aspirationMin),
            aspirationMax(aspitationMax), aspirationPlus(aspirationPlus), lifetime(lifetime){};
    ~TabuSearch();

   int solve();


};


#endif //PEA2_TABUSEARCH_H
