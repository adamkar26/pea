#include <iostream>
#include <fstream>
#include "Graph.h"
#include "TabuSearch.h"

using namespace std;

void test(){
    int t;
    TabuSearch* tb;
    string path;
    cout<<"Podaj ścieżkę do pliku: ";
    cin>>path;
    Graph*  graph = new Graph(path);

    int params[7];
    fstream file;
    file.open("./params.txt");
    if (!file.good()) perror(nullptr);
    for(int i=0; i<7 ;i++)
        file>>params[i];
    file.close();

    cout<< "Rodzaj algorytmu: "<<endl;
    cout<<"1. Tabu Search"<<endl;
    cin>>t;
    switch (t){
        case 1:
                tb = new TabuSearch(graph,params[0],params[1],params[2],params[3]
                        ,params[4],params[5],params[6]);
                tb->solve();
                delete tb;
                break;
            }

    delete graph;
}

void measure(){
    int t=0;
    string s;
    Graph* graph;
    cout<<"Podaj ścieżkę do grafu: ";
    cin>>s;
    graph = new Graph(s);
    fstream file;
    int params[7];
    file.open("./params.txt");
    if (!file.good()) perror(nullptr);
    for(int i=0; i<7 ;i++)
        file>>params[i];
    file.close();

    cout<< "Rodzaj algorytmu: "<<endl;
    cout<<"1. Tabu Search"<<endl;
    cin>>t;
    switch (t){
        case 1:
            TabuSearch *tb = new TabuSearch( graph, params[0], params[1], params[2], params[3], params[4],
                    params[5],params[6]);
            cout<<"Wynik: "<<tb->solve()<<endl;
            delete tb;
            break;
    }
    delete graph;

}
void cmd() {
    int t=0;
    while(t!=3)
    {
        cout << "Wybierz tryb pracy: " << endl;
        cout << "1. dobór paramterów " << endl;
        cout << "2. test " << endl;
        cout << "3. koniec "<<endl;
        cin >> t;
        switch (t) {
            case 1 : test(); break;
            case 2 : measure(); break;
        }
    }
}



int main() {
    srand(time(NULL));
    cmd();
    return 0;
}



