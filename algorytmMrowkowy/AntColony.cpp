//
// Created by Adam Karczewski on 2019-01-12.
//

#include <chrono>
#include <random>
#include <iostream>
#include <fstream>
#include "AntColony.h"

void AntColony::prepare() {

    bestPathValue =INT_MAX;
    bestPath.resize(graph->getSize());
    antPath.resize(m);
    antTabu.resize(m);
    phermones.resize(graph->getSize());
    antPathValue.resize(m);
    for(int i=0; i<graph->getSize();i++){
        phermones[i].resize(graph->getSize());
        for(int j=0; j<graph->getSize();j++){
            if(i==j)  phermones[i][j]=-1;
            phermones[i][j] = rho0;
        }
    }
    for(int i=0; i<m;i++){
        antPath[i].resize(graph->getSize());
        antTabu[i].resize(graph->getSize());

    }

}

void AntColony:: clear(){
    for(int i=0; i<m;i++)
        for (int j=0; j<graph->getSize();j++){
            antPath[i][j]=-1;
            antTabu[i][j]=false;
        }




     for(int i=0;i<m;i++)
         antPathValue[i]=0;

}

int AntColony::calculate(double maxTime) {

    string path="./wyniki_"+to_string(graph->getSize())+".csv";
    fstream file(path, ios::out);
    auto start = std::chrono::high_resolution_clock::now();
    prepare();
    while (true){
        auto end = chrono::high_resolution_clock::now();
        chrono::duration<double> diff = end - start;
        if(diff.count()>maxTime)   return bestPathValue;
        clear();
        for(int i=0; i<m; i++){

            // losowanie miasta startowego
            std::random_device rd;
            std::mt19937 g(rd());
            std::uniform_int_distribution<> dis(0, graph->getSize()-1);
            int prob = dis(g);
            antPath[i][0]=prob;
            antTabu[i][prob]= true;
        }

        // generowanie trasy
        for(int i=0; i<graph->getSize()-1;i++){

                for(int j=0;j<m;j++){
                    selectNextCity(i,j);
                }
        }



        for(int i=0; i<graph->getSize();i++){
            for(int j=0; j<graph->getSize();j++)
                phermones[i][j] -= rho*phermones[i][j];
        }

        //szukanie najlepszego wyniku
        for(int i=0; i<m;i++)
            if(bestPathValue>(antPathValue[i]+graph->getDistance(antPath[i][graph->getSize()-1],antPath[i][0]))){
                bestPathValue=antPathValue[i]+graph->getDistance(antPath[i][graph->getSize()-1],antPath[i][0]);
                bestPath=antPath[i];

                auto end = chrono::high_resolution_clock::now();
                chrono::duration<double> diff = end - start;
                file<<diff.count()<<", "<<bestPathValue<<endl;
            }

    }


}

void AntColony::selectNextCity(int iteration,int ant) {

    struct Prob{
        int v;
        double p;
    };
    vector<Prob> probability;
    double sum=0;
    for(int i=0; i<graph->getSize();i++) {
        if (antTabu[ant][i]) continue;
        else {
            double ni = (double) graph->getDistance(antPath[ant][iteration], i);
            ni = 1.0 / ni;
            Prob p;
            p.p  = pow(phermones[antPath[ant][iteration]][i], alpha) * pow(ni, beta);
            p.v=i;
            sum += p.p;
            probability.push_back(p);

        }
    }
    double use =0;
    for(int i=0; i<probability.size();i++){
        probability[i].p = probability[i].p/ sum;
        use += probability[i].p;
        probability[i].p=use;
    }

    std::random_device rd;
    std::mt19937 g(rd());
    std::uniform_int_distribution<> dis(0, 1000000);
    double value = (double)dis(g);
    value /= 1000000;
    int i=1;
    for(i=1; i<probability.size();i++){
        if(value<=probability[i].p && value>probability[i-1].p) {
            //jesli wylosowano ten wierzchołek
            antTabu[ant][probability[i].v] = true;
            antPath[ant][iteration+1]= probability[i].v;
            antPathValue[ant] += graph->getDistance(antPath[ant][iteration],antPath[ant][iteration+1]);

            //zostawienie śladu
            phermones[antPath[ant][iteration]][antPath[ant][iteration+1]] += q;
            if(!isATSP)phermones[antPath[ant][iteration]][antPath[ant][iteration]] += q;
            break;
        }
    }
    if(i==probability.size()){
        antTabu[ant][probability[0].v] = true;
        antPath[ant][iteration+1]= probability[0].v;
        antPathValue[ant] += graph->getDistance(antPath[ant][iteration],antPath[ant][iteration+1]);

        //zostawienie śladu
        phermones[antPath[ant][iteration]][antPath[ant][iteration+1]] += q;
        if(!isATSP)phermones[antPath[ant][iteration+1]][antPath[ant][iteration]] += q;
    }
}

AntColony::~AntColony(){

}