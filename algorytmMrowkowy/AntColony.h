//
// Created by Adam Karczewski on 2019-01-12.
//

#ifndef ALGORYTMMROWKOWY_ANTCOLONY_H
#define ALGORYTMMROWKOWY_ANTCOLONY_H

#include "Graph.h"


class AntColony {
private:
    double alpha;
    double beta;
    double rho;
    double q;
    int m;
    double rho0;
    Graph* graph;
    bool isATSP;
    vector<vector<bool>> antTabu;
    vector<vector<int>> antPath;
    vector<int> antPathValue;
    int bestPathValue;
    vector<int> bestPath;
    vector<vector<double>> phermones;

    void prepare();
    void clear();
    void selectNextCity(int iteration, int ant);

public:
    AntColony(double alpha, double beta, double rho, double q, int m, double rho0, Graph* graph, bool isATSP):
    alpha(alpha), beta(beta), rho(rho), m(m), rho0(rho0), graph(graph), isATSP(isATSP), q(q){};

    int calculate(double maxTime);

    ~AntColony();





};


#endif //ALGORYTMMROWKOWY_ANTCOLONY_H
