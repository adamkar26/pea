//
// Created by Adam Karczewski on 16/10/2018.
//
#include<iostream>
#include <string>
#include <fstream>
#include "Graph.h"
#include<stdio.h>
using namespace std;
Graph::Graph(string path) {

    ifstream file;
    file.open(path);
    if (!file.good()) perror(nullptr);
    file>>size;
    matrix = new int*[size];
    distance = new int*[size];
    for(int i=0; i<size; i++)
    {
        matrix[i] = new int [size];
        distance[i] = new int[2];
        for(int j=0; j<size; j++)
        {
            file>>matrix[i][j];
            if (matrix[i][j]== -1) matrix[i][j] = 0;
        }
    }
    file.close();
    calculateMin();

}

Graph::~Graph() {
    for(int i=0; i<size; i++)
    {
        delete[] matrix[i];
        delete[] distance[i];
    }
    delete[] matrix;
    delete[] distance;
}

int Graph::getDistance(int from, int to) {
    if (from>=size || to>=size) throw "Zly index";
    return matrix[from][to];
}

int Graph::getSize() {
    return size;
}

int Graph::getMinEdge(int from, bool out) {

    if(out) return distance[from][0];
    return distance[from][1];


}

void Graph::calculateMin() {

    for(int i=0; i<size; i++) {
        distance[i][0] = INT_MAX;  //minimalne wyjscie
        distance[i][1] = INT_MAX;  //minimalne wejscie
        for (int j = 0; j < size; j++) {
            if (j == i) continue;

            if (matrix[i][j] < distance[i][0]) distance[i][0] = matrix[i][j];
            if (matrix[j][i] < distance[i][1]) distance[i][1] = matrix[j][i];

        }
    }
}

bool Graph::isATSP() {
    for(int i=0;i<size;i++)
    {
        for(int j=0;j<size;j++)
            if(!(matrix[i][j]==matrix[j][i])) return true;
    }
    return false;
}

