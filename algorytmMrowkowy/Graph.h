//
// Created by Adam Karczewski on 16/10/2018.
//

#ifndef ALGORYTMMROWKOWY_GRAPH_H
#define ALGORYTMMROWKOWY_GRAPH_H
#include<string>
#include <vector>

using namespace std;



class Graph {

private:
    int** matrix;
    int size =-1;
    int** distance;
    void calculateMin();

public :
    Graph(string path);
   ~Graph();
    int getDistance(int form, int to);
    int getSize();
    int getMinEdge(int from, bool out);
    bool isATSP();




};


#endif //ALGORYTMMROWKOWY_GRAPH_H
