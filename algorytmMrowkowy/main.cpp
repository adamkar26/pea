#include <iostream>
#include "AntColony.h"
#include <fstream>
using namespace std;

void test(){
    int t;
    AntColony* ac;
    string path;
    Graph*  graph;
    double params[7];
    fstream file;
    file.open("./params.txt");
    if (!file.good()) perror(nullptr);
    for(int i=0; i<7 ;i++)
        file>>params[i];
    file.close();
    string paths[] ={"TSP/data120.txt"};
    double best[]={6905.0,6942.0,1163.0,2720.0,2755.0};
    cout<< "Rodzaj algorytmu: "<<endl;
    cout<<"1. Alogorytm mrówkowy"<<endl;
    cin>>t;
    switch (t){
        case 1:
            string out="./wynikiP.csv";
            fstream file1(out, ios::out);
            for(int k=0; k<1;k++){
                graph=new Graph(paths[k]);

                double r = graph->getSize();
               // r=r/best[k];
               // double r0[]={r,1,10,100,1000};
                int sum =0;
             //   for(int i=0;i<5;i++){

                   // for(int j=0;j<5;j++){

                        ac = new AntColony(params[0],params[1],params[2],params[3],graph->getSize(),params[5],graph,graph->isATSP());
                        cout<<ac->calculate(20)<<endl;
                        delete ac;
                  //  }

               // }
              //  sum = sum/5;
               // file1<<graph->getSize()<<", "<<sum<<endl;
            }


            break;
    }

    delete graph;
}

void measure(){
    int t=0;
    string s;
    Graph* graph;
    cout<<"Podaj ścieżkę do grafu: ";
    cin>>s;
    graph = new Graph(s);
    fstream file;
    int params[7];
    file.open("./params.txt");
    if (!file.good()) perror(nullptr);
    for(int i=0; i<7 ;i++)
        file>>params[i];
    file.close();

    cout<< "Rodzaj algorytmu: "<<endl;
    cout<<"1. Alogorytm mrówkowy"<<endl;
    cin>>t;
    switch (t){
        case 1:
            //if(graph->isATSP())  cout<<"ATSP"<<endl;

            AntColony* ac = new AntColony(params[0],params[1],params[2],params[3],params[4],params[5],graph,graph->isATSP());
            cout<< ac->calculate(params[6])<<endl;
            delete ac;
            break;
    }
    delete graph;

}
void cmd() {
    int t=0;
    while(t!=3)
    {
        cout << "Wybierz tryb pracy: " << endl;
        cout << "1. dobór paramterów " << endl;
        cout << "2. test " << endl;
        cout << "3. koniec "<<endl;
        cin >> t;
        switch (t) {
            case 1 : test(); break;
            case 2 : measure(); break;
        }
    }
}



int main() {
    srand(time(NULL));
    cmd();
    return 0;
}